<?php
	# ************ connectivity for mongo db Start ***************
	ini_set("display_errors", "1");
	error_reporting(E_ALL);
	$conn    = new MongoClient('localhost');
	$mongodb = $conn->users;

	# ************ connectivity for Mysql db Start ***************
	$conn = new mysqli('localhost', 'root', 'root', 'mydb');
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}


# ********************************************** PART ONE OVER All *************************************************************	


	# ************ Code for mango db getting count value ***************
	$currentTime          = date('Y-m-d', strtotime("-1 days")) . ' 00:00:00';
	//$currentTime = date('Y-m-d').' 00:00:00';
	//$currentTime = '2015-06-12 00:00:00';
	$prevdate             = date('Y-m-d', strtotime("-2 days")) . ' 00:00:00';
	//$prevdate='2015-06-11 00:00:00';
	$mongo_prevdate       = new MongoDate(strtotime($prevdate));
	$mongo_currentTime    = new MongoDate(strtotime($currentTime));
	MongoCursor::$timeout = -1;

	$collection = $mongodb->main_device;

	$device_count = $collection->find(array(
	    "updated_at" => array(
		'$gte' => $mongo_prevdate,
		'$lt' => $mongo_currentTime
	    )
	))->count();

	# ************ Code for inserting mongo count to mysql  ***************
	$insert = "INSERT INTO report_daily (date_from, date_to, count) VALUES ('$prevdate', '$currentTime', '$device_count')";
	
	if ($conn->query($insert) === TRUE) {
	    echo "New record created successfully";
	} else {
	    echo "Error: " . $insert . "<br>" . $conn->error;
	}



# ************************************** PART TWO UNITED STATES *************************************************************
	
	$where=array( '$and' => array( array('updated_at' =>array('$gte' => $mongo_prevdate,'$lt' => $mongo_currentTime)), array('country_code'=>'United States') ) );

	$device_data_us = $collection->find($where)->count();

	# ************ Code for inserting mongo count to mysql  ***************
	$insert_us = "INSERT INTO report_daily_us (date_from, date_to, count) VALUES ('$prevdate', '$currentTime', '$device_data_us')";
	
	if ($conn->query($insert_us) === TRUE) {
	    echo "New record created successfully";
	} else {
	    echo "Error: " . $insert_us . "<br>" . $conn->error;
	}
	print_r($device_data_us);
?>
