<?php
    ini_set("display_errors", "1");
    error_reporting(E_ALL);
    
    try {

            $conn = new Mongo('localhost');
            $mongodb = $conn->users;
            $collection_application = $mongodb->application; 
            $collection_uniapp = $mongodb->unique_apps; 

            $total_count = $collection_application->count();
            
            $no_of_records_per_page = 1000;    
            $test_val = 0;
            $no_of_pages = ceil($total_count/$no_of_records_per_page) ;
             
            for($i=0;$i<=$no_of_pages;$i++){
                $apps = $collection_application->find()->skip($no_of_records_per_page*$i)->limit($no_of_records_per_page);
                foreach($apps as $apps){

                    // code for adding unique value 
                    $app_data = array("app_name"=>$apps["app_name"]);
                    $find_app = $collection_uniapp->find($app_data)->count();

                    if(!$find_app){
                        $collection_uniapp->insert($app_data);
                    }
                }

            }

            echo "done";
        } catch (MongoConnectionException $e) {
                 die('Error connecting to MongoDB server');
        } catch (MongoException $e) {
                 die('Error: ' . $e->getMessage());
        }
        
        die;

    ?>
